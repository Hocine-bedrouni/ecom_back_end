package fr.insy2s.Commerce.repositories;

import fr.insy2s.Commerce.models.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

    Produit findProduitByLabel_produit(String label_produit);

    Produit findByLabel(String label);

    Produit findByPrix(double prix);

    Produit findAllByCategorie(Long id);

}
