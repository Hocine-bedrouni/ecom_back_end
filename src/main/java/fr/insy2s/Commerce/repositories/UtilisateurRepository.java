package fr.insy2s.Commerce.repositories;

import fr.insy2s.Commerce.models.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

     Utilisateur findByNom(String nomUtilsateur);

     Utilisateur findByEmail(String emailUtilisateur);
}
