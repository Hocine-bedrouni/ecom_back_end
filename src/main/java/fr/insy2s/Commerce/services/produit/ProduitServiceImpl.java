package fr.insy2s.Commerce.services.produit;

import fr.insy2s.Commerce.models.Produit;
import fr.insy2s.Commerce.repositories.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProduitServiceImpl implements ProduitService{

    @Autowired
    private ProduitRepository produitRepository;

    @Override
    public Optional<Produit> getProduitById(Long id) {
        return produitRepository.findById(id);
    }

    @Override
    public Produit getProduitByLabel(String label) {
        return produitRepository.findByLabel(label);
    }

    @Override
    public Produit getProduitByPrix(double prix) {
        return produitRepository.findByPrix(prix);
    }

    @Override
    public List<Produit> getAllProduits() {
        return produitRepository.findAll();
    }

//    @Override
//    public List<Produit> getProduitsByCategorieId(Long categorieId) {
//        return produitRepository.(categorieId);
//    }
}
