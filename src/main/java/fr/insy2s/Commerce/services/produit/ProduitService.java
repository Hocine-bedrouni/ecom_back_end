package fr.insy2s.Commerce.services.produit;

import fr.insy2s.Commerce.models.Produit;

import java.util.List;
import java.util.Optional;

public interface ProduitService {

    Optional<Produit> getProduitById(Long id);

    Produit getProduitByLabel(String label);

    Produit getProduitByPrix(double prix);

    List<Produit> getAllProduits();

    //On fait la recherche de catégorie produit par l'id_catégorie
    List <Produit> getProduitsByCategorieId(Long categorieId);

}
