package fr.insy2s.Commerce.services.utilisateur;

import fr.insy2s.Commerce.models.Utilisateur;
import fr.insy2s.Commerce.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class UtilisateurServiceImpl implements UtilisateurService{

    @Autowired
    private UtilisateurRepository utilisateurRepository;


    @Override
    public List<Utilisateur> getAllUtilisateurs() {
      return utilisateurRepository.findAll();
    }

    @Override
    public Optional<Utilisateur> getUtilsateurById(Long id) {
        return utilisateurRepository.findById(id);
    }

    @Override
    public Utilisateur getUtilisateurByName(String nomUtilisateur) {
        return utilisateurRepository.findByNom(nomUtilisateur);
    }

    @Override
    public Utilisateur getUtilisateurByEmail(String emailUtilisateur) {
        return utilisateurRepository.findByEmail(emailUtilisateur);
    }

    @Override
    public Utilisateur saveUtilisateur(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public void deleteById(Long id) {
        utilisateurRepository.deleteById(id);
    }
}
