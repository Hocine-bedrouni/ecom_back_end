package fr.insy2s.Commerce.services.utilisateur;

import fr.insy2s.Commerce.models.Utilisateur;

import java.util.List;
import java.util.Optional;

public interface UtilisateurService {

    List<Utilisateur> getAllUtilisateurs();

    Optional<Utilisateur> getUtilsateurById(Long id);

    Utilisateur getUtilisateurByName(String nomUtilisateur);

    Utilisateur getUtilisateurByEmail(String emailUtilisateur);

    Utilisateur saveUtilisateur(Utilisateur utilisateur);

    void deleteById(Long id);
}
